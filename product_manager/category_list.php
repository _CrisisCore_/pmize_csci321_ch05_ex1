<?php include '../view/header.php'; ?>
<main>

    <h1>Category List</h1>
    <table>
        <tr>
            <th>Name</th>
            <th>&nbsp;</th>
        </tr>
        <!-- Ex 5-1 step 7 -->
        <?php foreach ($categories as $category) : ?>
        <tr>
            <td><?php echo $category['categoryName']; ?></td>
            <td>
                <!-- reload index.php with a new value for the POSTed value for name="action" (note that this is different from the action attribute for the form itself) --> 
               
                <form action="index.php" method="post">
                    <!-- here's where $_POST['action'] is set to 
                    "delete category", which will tell the CONTROLLER 
                    what to do when we reload index.php -->
                    <input type="hidden" name="action" value="delete_category" />
                    <!-- here's where we store $_POST['category_id'] when the form
                    is POSTed --> 
                    <input type="hidden" name="category_id"
                           value="<?php echo $category['categoryID']; ?>"/>
                    <input type="submit" value="Delete"/>
                </form>
            </td>
        </tr>
        <?php endforeach; ?>
    </table>
    <!-- Start of Ex 5-1 step 8 --> 
    <h2>Add Category</h2>
    <form id="add_category_form"
          action="index.php" method="post">
        <!-- use a hidden input to set $_POST['action'] to 
        contain the value "add_category" --> 
        <input type="hidden" name="action" value="add_category" />

        <label>Name:</label>
        <input type="text" name="name" />
        <input type="submit" value="Add"/>
    </form>
    <!-- end of Exercise 5-1, step 8 --> 

    <p><a href="index.php?action=list_products">List Products</a></p>

</main>
<?php include '../view/footer.php'; ?>